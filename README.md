# sailplane

A tool to download videos or scrape an entire channel from floatplane.com. Requires a subscription, obviously.

## Screenshot

![Screenshot of the tool running](https://elixi.re/i/4sr08mql.png)

## Setup

- Clone or download the repo
- Install python3.6 or higher if you don't have it installed already. 3.8 or higher is recommended, or else you may need to edit the source code. Search "3.8" on sailplane.py.
- Install the requirements on `requirements.txt` with `pip3 install -Ur requirements.txt --user`
- Copy `config.template.json` to `config.json`, fill in `sails.sid` from your browser's cookies.

## Usage

After going through setup, run it as `sailplane.py video-id/channel-id/channel-name`.

Examples:
- `python3.7 sailplane.py L0zozuj2lW`: Downloads https://www.floatplane.com/video/L0zozuj2lW to `L0zozuj2lW.mp4`.
- `python3.7 sailplane.py 59f94c0bdd241b70349eb72b`: Downloads all videos by channel LinusTechTips.
- `python3.7 sailplane.py linustechtips`: Downloads all videos by channel https://www.floatplane.com/channel/linustechtips/home. Do keep in mind that this will not work if channel's name length is 10 or 24. Why? Because I'm lazy.

## Personal note to floatplane

Thank you for not having DRM. That's a genuinely good thing. Your API is very clear to work with, which is <3.

The dedicated download button is cool. I wrote this just so that I could do that in an automated way, so that I can binge on LTT content when I'm on the go without a stable internet connection.

Also, thank you for offering a good service. I really wish more creators were on floatplane though, and a subscription that gives access to all creators could be very nice. $5 for LTT alone seems a bit high.

## Disclaimer

This project is not affiliated with floatplane or LMG, it is also not endorsed by them.

Please do not use this software for illegal purposes like piracy, but rather for legal uses like personal backups (if it is legal in your jurisdiction-- it is in mine).

Usage of this tool might be a violation of floatplane ToS, that's your responsibility.

I don't accept any responsibility whatsoever.

