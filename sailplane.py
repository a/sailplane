import os
import sys
import json
import logging
import requests
from slugify import slugify


def download_file(url, filename):
    # Based on http://masnun.com/2016/09/18/
    # python-using-the-requests-module-to-download-large-files-efficiently.html
    os.makedirs("out", exist_ok=True)
    filename = os.path.join("out", filename)
    if os.path.exists(filename):
        logging.info(f"{filename} is already downloaded.")
        return True

    resp = requests.get(url, stream=True)
    if resp.status_code != 200:
        return False
    handle = open(filename, "wb")
    for chunk in resp.iter_content(chunk_size=512):
        if chunk:  # filter out keep-alive new chunks
            handle.write(chunk)

    logging.info(f"Done downloading {filename}.")
    return True


def download_video(video_id, sails_sid, user_agent, out_filename):
    url = f"https://www.floatplane.com/api/cdn/delivery?type=download&guid={video_id}"
    out = requests.get(
        url, cookies={"sails.sid": sails_sid}, headers={"User-Agent": user_agent},
    )
    outj = out.json()
    # TODO: check allowDownload
    edge = outj["edges"][-1]["hostname"]
    token = outj["resource"]["data"]["token"]
    highest_q = outj["resource"]["data"]["qualityLevels"][-1]["name"]
    uri = (
        outj["resource"]["uri"]
        .replace("{qualityLevels}", highest_q)
        .replace("{token}", token)
    )
    download_file(f"https://{edge}{uri}", out_filename)


def list_channel_videos(channel_id, sails_sid, user_agent):
    url = f"https://www.floatplane.com/api/creator/videos?creatorGUID={channel_id}"
    out = requests.get(
        url, cookies={"sails.sid": sails_sid}, headers={"User-Agent": user_agent},
    )
    outj = out.json()
    videos = {}
    for video in outj:
        if video["hasAccess"]:
            videos[slugify(video["title"])] = video["guid"]
    return videos


def channel_name_query(name, sails_sid, user_agent):
    url = f"https://www.floatplane.com/api/creator/named?creatorURL={name}"
    out = requests.get(
        url, cookies={"sails.sid": sails_sid}, headers={"User-Agent": user_agent},
    )
    return out.json()[0]["id"]


if __name__ == "__main__":
    print("sailplane, a tool to download videos from floatplane")
    print("GPLv3 | https://gitlab.com/a/sailplane")
    print(
        "Please do not use this software for illegal purposes like piracy, but rather for legal uses like personal backups (if it is legal in your jurisdiction-- it is in mine)."
    )
    logging.basicConfig(level=logging.INFO)

    if len(sys.argv) < 2:
        logging.error("No video or channel ID supplied.")

    in_id = sys.argv[1]

    # Account for no config
    if not os.path.exists("config.json"):
        logging.error(
            "No config found, please create one based on config.template.json."
        )
        sys.exit(1)

    # Read config
    with open("config.json", "r") as f:
        config = json.load(f)

    auth_token = config["sails.sid"]
    user_agent = config["user-agent"]

    if len(in_id) == 10:
        out_filename = f"{in_id}.mp4"
        download_video(in_id, auth_token, out_filename)
    else:  # channel is len 24
        if len(in_id) != 24:
            # This Breaks Tm if the channel name is 10 or 24 chars long, but meh
            in_id = channel_name_query(in_id, auth_token, user_agent)
        # If you're on anything less than python3.8 use these:
        # videos = list_channel_videos(in_id, auth_token, user_agent)
        # for video in videos:
        for video in (videos := list_channel_videos(in_id, auth_token, user_agent)) :
            video_id = videos[video]
            out_filename = f"{video}.mp4"
            download_video(video_id, auth_token, user_agent, out_filename)

    logging.info(f"All done, bye!")
